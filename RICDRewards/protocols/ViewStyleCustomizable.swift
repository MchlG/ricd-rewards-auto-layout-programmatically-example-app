//
//  CustomStyleable.swift
//  RICDRewards
//
//  Created by Mike on 7/27/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit

protocol ViewStyleCustomizable where Self: UIViewController {
    func styleViewBackground(color: UIColor?)
}

extension ViewStyleCustomizable {
    func styleViewBackground(color: UIColor?){
        view.backgroundColor = color ?? UIColor.white
    }
}
