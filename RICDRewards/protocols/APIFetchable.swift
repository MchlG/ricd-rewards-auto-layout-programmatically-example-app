//
//  APIFetchable.swift
//  RICDRewards
//
//  Created by Mike on 8/1/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import Foundation


protocol APIFetchable: class {
    var baseUrl: String {get}
    var defaultSession: URLSession {get}
    var dataTask: URLSessionTask? {get set}
    
    func fetchDataAtPath(_ path: String,  _ callback: @escaping (Data?, Error?)-> Void)
    func parseData(_ data: Data, _ callback: ([[String: Any]]?)-> Void)
}

extension APIFetchable {
    
    func fetchDataAtPath(_ path: String,  _ callback: @escaping (Data?, Error?)-> Void){
        
        dataTask?.cancel()
        
        if var urlComponents = URLComponents(string: baseUrl) {
            urlComponents.path = path
            
            guard let url = urlComponents.url else {return}
            
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer {self.dataTask =  nil}
                
                if let error = error {
                    callback(nil, error)
                } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    callback(data, nil)
                }
            }
            dataTask?.resume()
        }
    }
    
    func parseData(_ data: Data, _ callback: ([[String: Any]]?)-> Void){
        do {
            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
            if let dictionaries = jsonResponse as? [[String: Any]] {
                callback(dictionaries)
            } else {
                callback(nil)
            }
        } catch let error {
            print("parse error! = \(error.localizedDescription)")
            callback(nil)
        }
    }
    
}
