//
//  Prizes.swift
//  RICDRewards
//
//  Created by Mike on 7/28/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit

struct Prize: Comparable {
    
    var name: String
    var image: UIImage?
    var price: Int
    
    init(name: String, image: UIImage?, price: Int){
        self.name = name
        self.image = image
        self.price = price
    }
    
    var description: String {
        return name + " - $" + String(describing: price)
    }
    
    //conform to equatable
    static func == (lhs: Prize, rhs: Prize)-> Bool {
        return lhs.name == rhs.name && lhs.price == rhs.price
    }
    //conform to comparable
    static func <(lhs: Prize, rhs: Prize)-> Bool {
        return lhs.price < rhs.price
    }
    
}
