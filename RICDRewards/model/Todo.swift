//
//  Todo.swift
//  RICDRewards
//
//  Created by Mike on 8/1/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import Foundation

struct Todo {
    private let titleKey = "title"
    private let userIdKey = "userId"
    private let idKey = "id"
    private let completedKey = "completed"
    
    var title: String
    var userId: Int
    var id: Int
    var completed: Bool
    
    var completedString: String {
        return completed ? "completed" : "not completed"
    }
    
    init(dict: [String: Any]){
        guard let title = dict[titleKey] as? String, let userId = dict[userIdKey] as? Int, let id = dict[idKey] as? Int, let completed = dict[completedKey] as? Int else {
            self.title = "INVALID"
            self.userId = -1
            self.id = -1
            self.completed = false
            return
        }
        self.title = title
        self.userId = userId
        self.id = id
        self.completed = completed == 1
    }
    
    init(title: String, userId: Int, id: Int, completed: Int){
        self.title = title
        self.userId = userId
        self.id = id
        self.completed = completed == 1
    }
    
    static func todosFromArray(arr: [[String: Any]])-> [Todo]? {
        var allTodos = [Todo]()
        for todoDict in arr {
            let newTodo = Todo(dict: todoDict)
            allTodos.append(newTodo)
        }
        if allTodos.count > 0 {
            return allTodos
        }
        return nil
    }
}
