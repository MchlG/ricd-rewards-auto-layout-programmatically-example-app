//
//  PromotionsVCController.swift
//  RICDRewards
//
//  Created by Mike on 7/30/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import Foundation


class PromotionsVCController: APIFetchable {
    
    let baseUrl = "https://jsonplaceholder.typicode.com"
    let defaultSession =  URLSession(configuration: .default)
    var dataTask: URLSessionTask?
    
    func fetchTodosFromAPI(_ callback: @escaping ([Todo]?)-> Void){
        fetchDataAtPath("/todos") { data, error in
            if let error = error {
                print("error = \(error.localizedDescription)")
            } else if let data = data {
                self.parseData(data) { result in
                    if let result = result {
                        print("result = \(result)")
                        let allTodos = Todo.todosFromArray(arr: result)
                        callback(allTodos)
                    } else {
                        print("result is nil!")
                        callback(nil)
                    }
                }
            }
        }
    }
    
}







