//
//  PrizesVCController.swift
//  RICDRewards
//
//  Created by Mike on 7/29/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit

class PrizesVCController {
    
    //dummy method. In real app this would likely fetch remote data 
    func getAllPrizes(_ callback: ([Prize]?)-> Void) {
        let p1 = Prize(name: "Candy Floss", image: UIImage(named: "Candy Floss"), price: 12)
        let p2 = Prize(name: "Candy Cane", image: UIImage(named: "Candy Cane"), price: 14)
        let p3 = Prize(name: "Caramel", image: UIImage(named: "Caramel"), price: 16)
        let p4 = Prize(name: "Chocolate Bar", image: UIImage(named: "Chocolate Bar"), price: 18)
        let p5 = Prize(name: "Chocolate Chip", image: UIImage(named: "Chocolate Chip"), price: 20)
        let p6 = Prize(name: "Chocolate Egg", image: UIImage(named: "Chocolate Egg"), price: 22)
        let p7 = Prize(name: "Dark Chocolate", image: UIImage(named: "Dark Chocolate"), price: 24)
        let p8 = Prize(name: "Gummi Bear", image: UIImage(named: "Gummi Bear"), price: 26)
        let p9 = Prize(name: "Jaw Breaker", image: UIImage(named: "Jaw Breaker"), price: 28)
        let p10 = Prize(name: "Jelly Beans", image: UIImage(named: "Jelly Beans"), price: 30)
        let p11 = Prize(name: "Liquorice", image: UIImage(named: "Liquorice"), price: 32)
        let p12 = Prize(name: "Lollipop", image: UIImage(named: "Lollipop"), price: 34)
        let p13 = Prize(name: "Sour Chew", image: UIImage(named: "Sour Chew"), price: 36)
        let p14 = Prize(name: "Toffee Apple", image: UIImage(named: "Toffee Apple"), price: 38)
        callback([p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14])
    }
    
    
}
