//
//  CustomColors.swift
//  RICDRewards
//
//  Created by Mike on 7/27/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit


extension UIColor {
    static let ricdBlue = UIColor(red: 135/255, green: 206/255, blue: 250/255, alpha: 1.0)
}
extension UIFont {
    static func ricdLabelFont(ofSize size: CGFloat)-> UIFont {
        return UIFont(name: "helvetica", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    static func ricdMainLabelFont(ofSize size: CGFloat)-> UIFont {
        return UIFont(name: "helvetica-bold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    static func ricdButtonFont(ofSize size: CGFloat)-> UIFont {
        return UIFont(name: "helvetica-light", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}

