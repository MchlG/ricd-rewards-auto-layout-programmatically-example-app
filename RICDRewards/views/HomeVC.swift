//
//  HomeVC.swift
//  RICDRewards
//
//  Created by Mike on 7/27/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit


class HomeVC: UIViewController {
    
    //UI Elements
    private lazy var scrollView: UIScrollView = { [unowned self] in
        let view = UIScrollView()
        view.contentSize.height = self.view.frame.size.height
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let patientNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Jane Doe"
        label.textAlignment = .center
        label.font = UIFont.ricdMainLabelFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.ricdBlue.cgColor
        label.layer.cornerRadius = 12
        return label
    }()
    private let pointsLabel: UILabel = {
        let label = UILabel()
        label.text = "Points"
        label.textAlignment = .center
        label.font = UIFont.ricdLabelFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let pointsNumberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.ricdLabelFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let cashLabel: UILabel = {
        let label = UILabel()
        label.text = "Cash Value"
        label.textAlignment = .center
        label.font = UIFont.ricdLabelFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let cashNumberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.ricdLabelFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let coinImageView: UIImageView = {
        let imView = UIImageView()
        imView.image = UIImage(named: "gold-coin")
        imView.contentMode = .scaleAspectFit
        imView.alpha = 0
        imView.translatesAutoresizingMaskIntoConstraints = false
        return imView
    }()
    private let patientSinceLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.ricdLabelFont(ofSize: 20)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let lastVisitLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.ricdLabelFont(ofSize: 20)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let pointsStackView: UIStackView = { //contains pointsLabel and pointsNumberLabel
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        return stack
    }()
    private let cashStackView: UIStackView = {  //contains cashLabel and cashNumberLabel
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        return stack
    }()
    private let cashAndPointsStackView: UIStackView = { //contains pointsStackView and cashStackView
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    private let patientSinceAndLastVisitStackView: UIStackView = { // contains patientsSinceLabel and lastVisitLabel
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        return stack
    }()
    private let backgroundColorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.ricdBlue
        v.layer.cornerRadius = 12
        v.layer.masksToBounds = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    private let emojiLabel: UILabel = {
        let label = UILabel()
        label.text = "😁"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 36)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private var patientNameLabelTopAnchor: NSLayoutConstraint?
    
    //vars
    private var points: Int = 120
    private var cashValue: Int {
        return points/10
    }
    private var patientSinceYear: Int = 2007
    private var lastVisitDate: String = "June 4, 2018"
    private var didAnimateView = false
    
//    override func loadView() {
        /*APPLE Docs say to never call this method directly.
         https://developer.apple.com/documentation/uikit/uiviewcontroller/1621454-loadview
         Although I have read articles stating that Apple reccomends using this method to create a view and assign it to the UIViewController's view property for when NOT using storyboards ie. like we are here. Either way, it seems safe to not use this method and setup the view in viewDidLoad() since loadView() is called before viewDidLoad() and if there is no associated storyboard or nib then a UIView is assigned to the UIViewController's view property anywway.
         */
//    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        styleViewBackground(color: nil)
        layoutViewElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = ""
        setTextOnLabels()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !didAnimateView {
            animateCoinImage()
            animatePatientNameLabel()
            didAnimateView = true
        }
    }
    
    private func setTextOnLabels(){
        pointsNumberLabel.text = String(describing: points)
        cashNumberLabel.text = "$" + String(describing: cashValue)
        patientSinceLabel.text = "Patient Since: " + String(describing: patientSinceYear)
        lastVisitLabel.text = "Last Visit: " + lastVisitDate
    }
    
    private func animateCoinImage(){
        UIView.animate(withDuration: 3, animations: {()-> Void in
            self.coinImageView.alpha = 1.0
        }, completion: nil)
    }
    
    private func animatePatientNameLabel(){
        patientNameLabelTopAnchor?.isActive = false
        patientNameLabelTopAnchor = patientNameLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 60)
        patientNameLabelTopAnchor?.isActive = true
        
        UIView.animate(withDuration: 3, delay: 0.3, usingSpringWithDamping: 0.4, initialSpringVelocity: 3, options: .curveEaseOut, animations: {()-> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
        
       
    }
    
    
}






//View Layout Methods
extension HomeVC {
    
    private func layoutViewElements(){
        layoutScrollView()
        layoutPatientNameLabel()
        layoutPointsStackView()
        layoutCashStackView()
        layoutCashAndPointsStackView()
        layoutPatientSinceAndLastVisitStackView()
        layoutBackgroundColorView()
    }
    
    private func layoutScrollView(){
        view.addSubview(scrollView)
        scrollView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    private func layoutPatientNameLabel(){
        scrollView.addSubview(patientNameLabel)
        
        patientNameLabel.widthAnchor.constraint(equalToConstant: view.bounds.width-10).isActive = true
        patientNameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        patientNameLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true

        patientNameLabelTopAnchor = patientNameLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: -60)
        patientNameLabelTopAnchor?.isActive = true
    }
    
    private func layoutPointsStackView() {
        pointsLabel.widthAnchor.constraint(equalToConstant: 110).isActive = true
        pointsLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        pointsNumberLabel.widthAnchor.constraint(equalToConstant: 75).isActive = true
        pointsNumberLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        pointsStackView.addArrangedSubview(pointsLabel)
        pointsStackView.addArrangedSubview(pointsNumberLabel)
    }
    
    private func layoutCashStackView(){
        cashLabel.widthAnchor.constraint(equalToConstant: 110).isActive = true
        cashLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        cashNumberLabel.widthAnchor.constraint(equalToConstant: 75).isActive = true
        cashNumberLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        cashStackView.addArrangedSubview(cashLabel)
        cashStackView.addArrangedSubview(cashNumberLabel)
    }
    
    private func layoutCashAndPointsStackView(){
        scrollView.addSubview(cashAndPointsStackView)
        
        cashAndPointsStackView.addArrangedSubview(pointsStackView)
        cashAndPointsStackView.addArrangedSubview(coinImageView)
        cashAndPointsStackView.addArrangedSubview(cashStackView)
        
        //NOTE:
        /*when I try to put the following 3 lines in the closure where cashAndPointsStackView is initialized it causes constraints to break and warnings in debugger. This is only the case for this stack view and not the other 3*/
        cashAndPointsStackView.alignment = .center
        cashAndPointsStackView.axis = .horizontal
        cashAndPointsStackView.distribution = .equalCentering
        //
        
        cashAndPointsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        cashAndPointsStackView.topAnchor.constraint(equalTo: patientNameLabel.bottomAnchor, constant: 100).isActive = true
        cashAndPointsStackView.widthAnchor.constraint(equalToConstant: view.bounds.width - 20).isActive = true
        cashAndPointsStackView.heightAnchor.constraint(equalToConstant: 110).isActive = true
        cashAndPointsStackView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 10).isActive = true
        cashAndPointsStackView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -10).isActive = true
    }
    
    private func layoutPatientSinceAndLastVisitStackView(){
        scrollView.addSubview(patientSinceAndLastVisitStackView)
        
        patientSinceLabel.widthAnchor.constraint(equalToConstant: view.bounds.width*0.95).isActive = true
        patientSinceLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        emojiLabel.widthAnchor.constraint(equalToConstant: view.bounds.width*0.95).isActive = true
        emojiLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        lastVisitLabel.widthAnchor.constraint(equalToConstant: view.bounds.width*0.95).isActive = true
        lastVisitLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        patientSinceAndLastVisitStackView.addArrangedSubview(patientSinceLabel)
        patientSinceAndLastVisitStackView.addArrangedSubview(emojiLabel)
        patientSinceAndLastVisitStackView.addArrangedSubview(lastVisitLabel)
        patientSinceAndLastVisitStackView.heightAnchor.constraint(equalToConstant: 85).isActive = true
        patientSinceAndLastVisitStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        patientSinceAndLastVisitStackView.topAnchor.constraint(greaterThanOrEqualTo: cashAndPointsStackView.bottomAnchor, constant: 100).isActive = true //NOTE: On iPhone Plus sizes, this stack view is too far off the bottom. How do I pin it to the bottom of the view but also ensure that it is some distance from the stackview that is above it? Having trouble doing this inside the scroll view
    }
    
    private func layoutBackgroundColorView(){
        patientSinceAndLastVisitStackView.insertSubview(backgroundColorView, at: 0)
        backgroundColorView.topAnchor.constraint(equalTo: patientSinceAndLastVisitStackView.topAnchor).isActive = true
        backgroundColorView.bottomAnchor.constraint(equalTo: patientSinceAndLastVisitStackView.bottomAnchor).isActive = true
        backgroundColorView.leftAnchor.constraint(equalTo: patientSinceAndLastVisitStackView.leftAnchor).isActive = true
        backgroundColorView.rightAnchor.constraint(equalTo: patientSinceAndLastVisitStackView.rightAnchor).isActive = true
    }
    
}







//Customize the View's style
extension HomeVC: ViewStyleCustomizable {
    //uses default implementation provided by ViewStyleCustomizable extension
}


