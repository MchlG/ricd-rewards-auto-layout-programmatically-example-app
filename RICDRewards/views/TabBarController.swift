//
//  TabBarController.swift
//  RICDRewards
//
//  Created by Mike on 7/27/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addViewControllerToTabBar()
        self.styleTabBar()
    }
    
    private func addViewControllerToTabBar(){
        let homeVC = HomeVC()
        homeVC.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "tabicon-home"), selectedImage: nil)
        
        //let prizesVC = PrizesVC(collectionViewLayout: UICollectionViewFlowLayout()) //collection view must specify a layout!
        let prizesVC = PrizesVC()
        prizesVC.tabBarItem = UITabBarItem(title: "Prizes", image: UIImage(named: "tabicon-star"), selectedImage: nil)
        
        let promotionsVC = PromotionsVC()
        promotionsVC.tabBarItem = UITabBarItem(title: "Promotions", image: UIImage(named: "tabicon-people"), selectedImage: nil)
        
        let tabs = [homeVC, prizesVC, promotionsVC] as [UIViewController]
        self.viewControllers = tabs
    }
    
    private func styleTabBar(){
        self.tabBar.tintColor = UIColor.ricdBlue
    }

}





extension TabBarController {
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("memory warning in \(self.debugDescription)")
    }
}
