//
//  PrizesVC.swift
//  RICDRewards
//
//  Created by Mike on 7/27/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit

private enum SortState: String {
    case highestFirst = "Sort by price highest -> lowest"
    case lowestFirst = "Sort by price lowest -> highest"
}

class PrizesVC: UIViewController {
    
    //UI Elements
    private lazy var collectionView: UICollectionView = { [unowned self] in
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: self.view.bounds.width*0.05, bottom: 10, right: self.view.bounds.width*0.05)
        layout.itemSize = CGSize(width: 50, height: 50)
        let cView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        cView.bounces = true
        cView.isScrollEnabled = true
        cView.alwaysBounceVertical = true
        cView.showsVerticalScrollIndicator = true
        cView.delegate = self
        cView.dataSource = self
        cView.backgroundColor = UIColor.white
        cView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: K.CellIdentifiers.custom_collectionview_cell)
        cView.translatesAutoresizingMaskIntoConstraints = false
        return cView
    }()
    private lazy var sortButton: UIButton = { [unowned self] in
        let button = UIButton()
        button.titleLabel?.font = UIFont.ricdButtonFont(ofSize: 20)
//        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .ricdBlue
        button.setTitle(currentSortState.rawValue, for: .normal)
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(self.sortButtonTapped(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    //vars
    private var sortButtonBottomAnchorConstraint: NSLayoutConstraint?
    private var currentSortState: SortState = .highestFirst
    private let controller = PrizesVCController()
    private var allPrizes: [Prize]?
    
//    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)   {
//        allPrizes = controller.getAllPrizes()
//        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
//    }
//    required init?(coder aDecoder: NSCoder) {
//        allPrizes = controller.getAllPrizes()
//        super.init(coder: aDecoder)
//    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        styleViewBackground(color: nil)
        layoutCollectionView()
        layoutSortButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "Prizes"
        fetchPrizes()
    }
    
    private func fetchPrizes(){
        controller.getAllPrizes() { prizes in
            self.allPrizes = prizes
            self.collectionView.reloadData()
        }
    }
    
    @objc private func sortButtonTapped(_ sender: UIButton) {
        toggleSortState()
        reloadAndScrollTopCollectionView()
    }
    
    private func toggleSortState(){
        if currentSortState == .highestFirst {
            currentSortState = .lowestFirst
            allPrizes = allPrizes?.sorted().reversed()
        } else {
            currentSortState = .highestFirst
            allPrizes?.sort()
        }
        animateSortButtonMovement()
        animatedSortButtonTitleChange()
    }
    
    private func reloadAndScrollTopCollectionView(){
        self.collectionView.reloadData()
        self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    private func animateSortButtonMovement(){
        guard let bottomAnchor = sortButtonBottomAnchorConstraint else {
            return
        }
        bottomAnchor.isActive = false
        if bottomAnchor.constant == -10 {
            bottomAnchor.constant = -50
        } else {
            bottomAnchor.constant = -10
        }
        bottomAnchor.isActive = true
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 3, options: .curveEaseOut, animations: {()-> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    private func animatedSortButtonTitleChange(){
        UIView.transition(with: sortButton, duration: 0.2, options: .transitionFlipFromTop, animations: {()-> Void in
            self.sortButton.setTitle(self.currentSortState.rawValue, for: .normal)
        }, completion: nil)
    }
    
}

// View layout methods
extension PrizesVC {
    
    private func layoutCollectionView(){
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
    }
    
    private func layoutSortButton(){
        view.addSubview(sortButton)
        sortButtonBottomAnchorConstraint = sortButton.bottomAnchor.constraint(equalTo: collectionView.topAnchor, constant: -10)
        sortButtonBottomAnchorConstraint?.isActive = true
        
        sortButton.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 20).isActive = true
        sortButton.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -20).isActive = true
    }
    
}




//Customize the View's style
extension PrizesVC: ViewStyleCustomizable {
    //uses default implementation provided by ViewStyleCustomizable extension
}




//UICollectionView methods
extension PrizesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPrizes?.count ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath)-> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: K.CellIdentifiers.custom_collectionview_cell, for: indexPath)
        guard let customCell = cell as? CustomCollectionViewCell else {
            print("Error - failed to downcast UICollectionViewCell to CustomCollectionViewCell in \(self.debugDescription)")
            cell.backgroundColor = .red
            return cell
        }
        if let prizes = allPrizes {
            customCell.mainLabel.text = prizes[indexPath.row].description
            customCell.imageView.image = prizes[indexPath.row].image
        } else {
            customCell.mainLabel.text = "No prizes :("
            customCell.imageView.image = nil
        }
        return customCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected \(indexPath.row)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2.5
        let height = collectionView.bounds.width/2.5
        let cellSize: CGSize = CGSize(width: width, height: height)
        
        return cellSize
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30    }
    
}



