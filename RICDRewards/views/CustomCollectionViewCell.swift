//
//  CustomCollectionViewCell.swift
//  RICDRewards
//
//  Created by Mike on 7/28/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    var mainLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.ricdMainLabelFont(ofSize: 15)
        label.textAlignment = .center
        label.numberOfLines = 1
        label.minimumScaleFactor = 0.1
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    var imageView: UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //style cell
        layer.borderWidth = 2
        layer.borderColor = UIColor.ricdBlue.cgColor
        layer.cornerRadius = 12
        layer.masksToBounds = true
        
        //add subviews
        self.addSubview(mainLabel)
        self.addSubview(imageView)
        
        //layout mainLabel
        mainLabel.topAnchor.constraintGreaterThanOrEqualToSystemSpacingBelow(self.safeAreaLayoutGuide.topAnchor, multiplier: 1).isActive = true
        mainLabel.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -5).isActive = true
        mainLabel.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: -5).isActive = true
        mainLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 5).isActive = true
        
        //layout imageView
        imageView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 5).isActive = true
        imageView.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 5).isActive = true
        imageView.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: -5).isActive = true
        imageView.bottomAnchor.constraint(equalTo: mainLabel.topAnchor, constant: -5).isActive = true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
}



