//
//  PromotionsVC.swift
//  RICDRewards
//
//  Created by Mike on 7/30/18.
//  Copyright © 2018 MikeGordonDeveloperOrganization. All rights reserved.
//

import UIKit


class PromotionsVC: UIViewController {
    
    //TODO:
    /*
     -add UI above table view
     -customize tableview cell
     -add loading indicator
     */
    
    private lazy var tableView: UITableView = { [unowned self] in
       let tView = UITableView()
        tView.delegate = self
        tView.dataSource = self
        tView.register(UITableViewCell.self, forCellReuseIdentifier: "cellID")
        tView.backgroundColor = .white
        tView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        tView.separatorColor = UIColor.ricdBlue
        tView.rowHeight = 90
        tView.translatesAutoresizingMaskIntoConstraints = false
        return tView
    }()
    
    private var allTodos: [Todo]?
    private let controller = PromotionsVCController()

    override func viewDidLoad(){
        super.viewDidLoad()
        styleViewBackground(color: nil)
        layoutTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "Promotions"
        fetchTodos()
    }
    
    private func fetchTodos(){
        controller.fetchTodosFromAPI() { todos in
            self.allTodos = todos
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    
}


//View layout methods
extension PromotionsVC {
    
    private func layoutTableView(){
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: view.bounds.height/2),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)
        ])
    }
    
    
}


extension PromotionsVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView)-> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)-> Int {
        return allTodos?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
        guard let todos = allTodos else {
            cell.textLabel?.text = "No Todos"
            return cell
        }
        cell.textLabel?.text = todos[indexPath.row].title
        cell.detailTextLabel?.text = todos[indexPath.row].completedString
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("did select row #\(indexPath.row)")
    }
    
}

extension PromotionsVC: ViewStyleCustomizable {
    //uses default implementatio provided by ViewStyleCustomizable extension
}


