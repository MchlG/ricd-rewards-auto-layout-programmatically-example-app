# RICD-Rewards-AutoLayout-Programmatically-Example-App


<strong>Task:</strong> 
</br>
Create an example app that uses iOS Auto Layout programmatically, no storyboards whatsoever. 
</br></br>



<strong> Key features and where to find them:</strong> 
</br>
. UIStackView(s) - HomeVC.swift 
</br>
. UIScrollView -  HomeVC.swift 
</br>
. UIView Animations by altering Auto Layout constraints - HomeVC.swift and PrizesVC.swift
</br>
. UICollectionView - PrizesVC.swift
</br></br>



<strong> Resources I used:</strong> 
</br>
-https://www.youtube.com/watch?v=9RydRg0ZKaI
</br>
-https://medium.com/@agoiabeladeyemi/programmatically-approach-auto-layout-2b97faa6b520
</br>
-https://www.youtube.com/watch?v=9cEMh19P-R4
</br>
-https://developer.apple.com/documentation/uikit/uiviewcontroller/1621454-loadview
</br>
-https://www.youtube.com/watch?v=d1e9bKtOEH8
</br>
-http://swiftdeveloperblog.com/code-examples/create-uicollectionview-in-swift-programmatically/
</br>
-https://stackoverflow.com/questions/39438803/how-to-create-uicollectionviewcell-programmatically
</br>
-https://medium.com/@jamesrochabrun/collection-views-are-similar-to-table-views-they-show-a-collection-of-cells-defined-by-a-custom-5b5084cca945
</br>
-https://useyourloaf.com/blog/stack-view-background-color/
</br>




